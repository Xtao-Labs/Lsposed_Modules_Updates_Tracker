import contextlib
import traceback
from asyncio import sleep
from os import remove
from random import uniform

from pyrogram.enums import ParseMode
from pyrogram.errors import FloodWait, ButtonUrlInvalid
from pyrogram.types import Message

from ci import channel_id, app, sqlite
from defs.msg import gen_update_msg
from defs.source import download
from defs.subs import send_to_subscribes
from defs.utils import Module


async def send_track_msg(file, track_msg) -> Message:
    if file:
        return await app.send_document(channel_id, file,
                                       caption=track_msg.text,
                                       force_document=True,
                                       parse_mode=ParseMode.HTML,
                                       reply_markup=track_msg.button)
    else:
        return await app.send_message(channel_id, track_msg.text,
                                      parse_mode=ParseMode.HTML,
                                      reply_markup=track_msg.button)


async def push_module_to_channel(i: Module):
    track_msg = gen_update_msg(i)
    msg = None
    if track_msg.url:
        try:
            file, url = await download(track_msg.url, track_msg.name, i.name)
            track_msg.url = url
            try:
                msg = await send_track_msg(file, track_msg)
            except FloodWait as e:
                print(f"Send document flood - Need wait for {e.value} second(s)")
                await sleep(e.value + uniform(0.5, 1.0))
                msg = await send_track_msg(file, track_msg)
            except ButtonUrlInvalid:
                print("Send button error")
                msg = await app.send_document(channel_id, file,
                                              caption=track_msg.text,
                                              force_document=True,
                                              parse_mode=ParseMode.HTML, )
            except Exception:
                traceback.print_exc()
            with contextlib.suppress(FileNotFoundError):
                remove(file)
        except FileNotFoundError:
            track_msg.url = None
    if not track_msg.url:
        try:
            msg = await send_track_msg(None, track_msg)
        except FloodWait as e:
            print(f"Send document flood - Sleep for {e.value} second(s)")
            await sleep(e.value + uniform(0.5, 1.0))
            msg = await send_track_msg(None, track_msg)
        except ButtonUrlInvalid:
            print("Send button error")
            msg = await app.send_message(channel_id, track_msg.text, parse_mode=ParseMode.HTML, )
        except Exception:
            traceback.print_exc()
    await sleep(uniform(0.5, 2.0))
    data_ = sqlite.get(i.name, {"msg_link": ""})
    if msg:
        data_["msg_link"] = msg.link
    else:
        data_["msg_link"] = "https://t.me/lsposed_Modules_Updates_Tracker"
    sqlite[i.name] = data_
    await send_to_subscribes(i)
