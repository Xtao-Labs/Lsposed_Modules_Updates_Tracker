from pyrogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from defs.utils import Module, TrackMessage
from ci import me

template = """
{}

<b>模块：</b><code>{}</code>
<b>简介：</b><code>{}</code>
<b>版本：</b><code>{}</code>
<b>更新时间：</b><code>{}</code>
<b>更新日志：</b>
<code>
{}
</code>
@lsposed_Modules_Updates_Tracker | @lsposed_Geeks_Bot
"""


def gen_button(data: Module) -> InlineKeyboardMarkup:
    data_ = []
    if data.releases:
        if data.releases[0].releaseAssets:
            data_ = [[InlineKeyboardButton("⬇️ 下载", url=data.releases[0].releaseAssets[0].url)]]
        data_.extend([[InlineKeyboardButton("Release", url=data.releases[0].url),
                       InlineKeyboardButton("主页", url=data.homepageUrl),
                       InlineKeyboardButton(
                           "订阅",
                           url=f"https://t.me/{me.username}?start={data.name.replace('.', '_')}"), ]])
    else:
        data_.extend([[InlineKeyboardButton("主页", url=data.homepageUrl),
                       InlineKeyboardButton(
                           "订阅",
                           url=f"https://t.me/{me.username}?start={data.name.replace('.', '_')}"), ]])
    return InlineKeyboardMarkup(data_)


def format_text(text: str) -> str:
    text = text.strip()
    for i in ["/", " ", "-", "@", "（", ]:
        text = text.replace(i, "_")
    for i in ["【", "】", "[", "]", "！", "(", ")", "`", "!", ]:
        text = text.replace(i, "")
    return text.strip()


def gen_tags(data: Module) -> str:
    text = f"#{format_text(data.description.split()[0])} "
    if data.collaborators:
        for i in data.collaborators:
            text += f"#{format_text(i)} "
    return text


def gen_update_msg(data: Module) -> TrackMessage:
    text = template.format(gen_tags(data), data.name, data.description, data.latestRelease,
                           data.updatedAt,
                           data.releases[0].description.replace(r"\r\n", "\n")[:250].strip())
    url = None
    name = None
    if data.releases and data.releases[0].releaseAssets:
        url = data.releases[0].releaseAssets[0].url
        mime = data.releases[0].releaseAssets[0].name.split(".")[-1:][0]
        name = data.name.replace('.', '_') + "-" + data.latestRelease + "." + mime
    button = gen_button(data)
    return TrackMessage(text, url, name, button)
