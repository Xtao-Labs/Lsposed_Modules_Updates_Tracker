from datetime import datetime, timedelta, timezone
date_format = "%Y-%m-%dT%H:%M:%SZ"


def strf_time(data: str) -> str:
    # data = "2021-07-17T09:14:05Z"
    if data:
        ts = datetime.strptime(data, date_format)
    else:
        return "未知"
    # UTC+8
    ts = ts + timedelta(hours=8)
    return ts.strftime("%Y/%m/%d %H:%M:%S")


def now_time() -> str:
    # UTC
    ts = datetime.now(timezone.utc)
    # UTC+8
    ts = ts + timedelta(hours=8)
    return ts.strftime("%Y/%m/%d %H:%M:%S")


def strp_time(data: str) -> datetime:
    # data = "2021-07-17T09:14:05Z"
    if data:
        ts = datetime.strptime(data, date_format)
    else:
        return datetime.now()
    # UTC+8
    ts = ts + timedelta(hours=8)
    return ts
