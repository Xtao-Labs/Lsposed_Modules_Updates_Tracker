import re
from datetime import datetime
from typing import List
from defs.format_time import strf_time, strp_time

REMOVE_HTML = re.compile(r"<[^>]+>", re.S)


class Assets:
    def __init__(self, data: dict):
        self.name = data["name"]
        self.url = data["downloadUrl"]


class Release:
    def __init__(self, data: dict):
        self.name: str = data["name"]
        self.url: str = data["url"]
        self.descriptionHTML: str = data.get("descriptionHTML", "")
        self.description = REMOVE_HTML.sub("", self.descriptionHTML).strip()
        self.publishedAt: str = strf_time(data["publishedAt"])
        self.publishedAtTime: datetime = strp_time(data["publishedAt"])
        self.tagName: str = data["tagName"]
        self.isPrerelease: bool = data["isPrerelease"]
        assets = []
        if data["releaseAssets"]:
            assets.extend(Assets(i) for i in data["releaseAssets"])
        self.releaseAssets: List[Assets] = assets
        self.releaseAssetsLen = len(assets)


class Module:
    def __init__(self, data: dict):
        self.name: str = data["name"]
        self.description: str = data["description"] or ""
        self.url: str = data["url"] or ""
        self.homepageUrl: str = data["homepageUrl"] or data["url"]

        self.sourceUrl: str = data["sourceUrl"] or ""
        self.hide: bool = data["hide"]
        self.createdAt: str = strf_time(data["createdAt"])
        text = []
        for i in data["collaborators"]:
            if i.get("name"):
                text.append(i["name"].replace(" ", "_").replace(".", "_").replace("-", "_"))
            else:
                text.append(i["login"].replace(" ", "_").replace(".", "_").replace("-", "_"))
        self.collaborators: List[str] = text
        self.latestRelease: str = data.get("latestRelease", "")
        releases = []
        if data["releases"]:
            releases.extend(Release(i) for i in data["releases"])
        if data.get("betaReleases"):
            releases.extend(Release(i) for i in data["betaReleases"])
        releases.sort(key=lambda x: x.publishedAtTime, reverse=True)
        self.releases: List[Release] = releases
        self.updatedAt: str = releases[0].publishedAt if releases else strf_time(data["updatedAt"])
        self.summary: str = data["summary"] or ""
        self.scope: List[str] = data["scope"] or []


class TrackMessage:
    def __init__(self, text, url, name, button):
        self.text = text
        self.url = url
        self.name = name
        self.button = button
