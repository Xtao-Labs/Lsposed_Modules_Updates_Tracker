from pyrogram import Client, filters
from pyrogram.types import Message

from ci import admin_id
from defs.source import from_keyword_to_module
from defs.track import push_module_to_channel


@Client.on_message(filters.incoming & filters.private & filters.chat(admin_id) &
                   filters.command(["force_push"]))
async def force_push(_: Client, message: Message):
    if len(message.command) != 2:
        await message.reply_text("Usage: /force_push <app_name>")
        return
    data = " ".join(message.command[1:])
    module = from_keyword_to_module(data)
    if not module:
        await message.reply_text("Not found this app.")
        return
    await push_module_to_channel(module)
    await message.reply("Force push OK!")
