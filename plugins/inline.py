from pyrogram import Client, emoji
from pyrogram.types import InlineQuery, InputTextMessageContent, InlineQueryResultArticle

from defs.source import new_modules, new_modules_index
from plugins.info import module_msg, gen_info_button


@Client.on_inline_query()
async def inline_process(_: Client, query: InlineQuery):
    data = []
    text = query.query.split()
    nums = 0
    if not new_modules_index:
        return
    data_ = new_modules_index
    for key, module in data_.items():
        if len(text) == 0:
            data.append(InlineQueryResultArticle(
                (module.summary if module.summary else module.description) + " - " + module.name,
                InputTextMessageContent(module_msg.format(
                    module.name,
                    module.description,
                    module.summary,
                    module.latestRelease,
                    module.createdAt,
                    module.updatedAt,
                    "\n    ".join(module.scope) if module.scope else "未声明",
                )),
                reply_markup=gen_info_button(module),
            ))
            nums += 1
        else:
            name = module.name + module.description + module.url + module.homepageUrl + module.summary + \
                   module.sourceUrl
            if module.scope:
                for i in module.scope:
                    name += i
            skip = any(i not in name for i in text)
            if not skip:
                data.append(InlineQueryResultArticle(
                    (module.summary if module.summary else module.description) + " - " + module.name,
                    InputTextMessageContent(module_msg.format(
                        module.name,
                        module.description,
                        module.summary,
                        module.latestRelease,
                        module.createdAt,
                        module.updatedAt,
                        "\n    ".join(module.scope) if module.scope else "未声明",
                    )),
                    reply_markup=gen_info_button(module),
                ))
                nums += 1
        if nums >= 50:
            break
    if nums == 0:
        return await query.answer(
            results=[],
            switch_pm_text=f'{emoji.CROSS_MARK} 字符串 "{" ".join(text)}" 没有搜索到任何结果',
            switch_pm_parameter="help",
        )
    await query.answer(data,
                       switch_pm_text=f'{emoji.KEY} 搜索了 {len(new_modules)} 个模块',
                       switch_pm_parameter="help", )
