from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton

from ci import sqlite, me
from defs.source import from_keyword_to_module
from defs.utils import Module
from plugins.start import not_found_msg

info_help_msg = """
👩🏻‍💼 » /info <code>包名|关键词|作用域|开源地址|模块地址</code> - 查询模块信息
     <code>/info nil.nadph.qnotified</code>
     <code>/info QNotified</code>
     <code>/info com.tencent.mobileqq</code>
     <code>/info https://github.com/ferredoxin/QNotified</code>
     <code>/info https://modules.lsposed.org/module/nil.nadph.qnotified</code>
"""
module_msg = """
<b>{}</b>

<b>简介：</b><code>{}</code>
<b>概要：</b><code>{}</code>
<b>版本：</b><code>{}</code>
<b>创建时间：</b><code>{}</code>
<b>更新时间：</b><code>{}</code>
<b>作用域：</b>
<code>
    {}
</code>
@lsposed_Modules_Updates_Tracker | @lsposed_Geeks_Bot
"""


def gen_info_button(data: Module) -> InlineKeyboardMarkup:
    msg_link = sqlite.get(data.name, {}).get("msg_link", "https://t.me/lsposed_Modules_Updates_Tracker")
    data_ = [[InlineKeyboardButton("详情", url=msg_link),
              InlineKeyboardButton("订阅",
                                   url=f"https://t.me/{me.username}?start={data.name.replace('.', '_')}"), ]]
    return InlineKeyboardMarkup(data_)


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["info"]))
async def info_command(_: Client, message: Message):
    if len(message.command) == 1:
        await message.reply(info_help_msg, quote=True)
    else:
        data = " ".join(message.command[1:])
        module = from_keyword_to_module(data)
        if module:
            await message.reply(
                module_msg.format(
                    module.name,
                    module.description,
                    module.summary,
                    module.latestRelease,
                    module.createdAt,
                    module.updatedAt,
                    "\n    ".join(module.scope) if module.scope else "未声明",
                ),
                reply_markup=gen_info_button(module),
                quote=True,
            )
        else:
            await message.reply(not_found_msg.format(data), quote=True)
