from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton

help_msg = """
下面是我学会了的指令列表：

👩🏻‍💼 » /subscribe <code>包名|关键词|作用域|开源地址|模块地址</code> - 订阅更新
     <code>/subscribe nil.nadph.qnotified</code>
     <code>/subscribe QNotified</code>
     <code>/subscribe com.tencent.mobileqq</code>
     <code>/subscribe https://github.com/ferredoxin/QNotified</code>
     <code>/subscribe https://modules.lsposed.org/module/nil.nadph.qnotified</code>

👩🏻‍💼 » /unsubscribe <code>包名|关键词|作用域|开源地址|模块地址</code> - 取消订阅更新

👩🏻‍💼 » /subscription - 列出您当前的订阅

👩🏻‍💼 » /info <code>包名|关键词|作用域|开源地址|模块地址</code> - 查询模块信息
"""


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["help"]))
async def help_command(_: Client, message: Message):
    await message.reply(
        help_msg,
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("订阅", callback_data="subs")]]
        ),
        disable_web_page_preview=True,
        quote=True,
    )
