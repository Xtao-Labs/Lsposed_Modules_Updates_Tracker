from asyncio import sleep
from random import uniform

from pyrogram.types import Message

from ci import scheduler, admin_id
from pyrogram import Client, filters
from defs.source import update_data, compare
from defs.track import push_module_to_channel


@scheduler.scheduled_job("cron", minute="*/30", id="0")
async def run_every_30_minute():
    await update_data()
    need_update = compare()
    for i in need_update:
        await push_module_to_channel(i)
        await sleep(uniform(0.5, 2.0))


@Client.on_message(filters.incoming & filters.private & filters.chat(admin_id) &
                   filters.command(["force_update", ]))
async def force_update(_: Client, message: Message):
    await run_every_30_minute()
    await message.reply("Force update complete OK!")
