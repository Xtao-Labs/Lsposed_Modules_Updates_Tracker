from pyrogram import Client, filters
from pyrogram.types import CallbackQuery, InlineKeyboardMarkup, InlineKeyboardButton

from defs.subs import gen_subs_msg, gen_back_button
from plugins.help import help_msg


@Client.on_callback_query(filters.regex("help"))
async def help_set(_, query: CallbackQuery):
    await query.edit_message_text(
        help_msg,
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("订阅", callback_data="subs")]]
        ),
        disable_web_page_preview=True,
    )


@Client.on_callback_query(filters.regex("subs"))
async def subs_set(_, query: CallbackQuery):
    text = gen_subs_msg(query.from_user.id)
    await query.edit_message_text(
        text,
        reply_markup=gen_back_button(),
        disable_web_page_preview=True,
    )
