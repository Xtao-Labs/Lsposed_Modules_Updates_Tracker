from pyrogram import Client, filters
from pyrogram.types import Message, InlineKeyboardMarkup, InlineKeyboardButton
from ci import me
from defs.source import from_name_to_module
from defs.subs import add_to_subs, remove_from_subs

des = """
你好！{} 我是 [{}]({})，一个为 lsposed 用户打造的一体化机器人！
我可以帮助你获取最新的 lsposed 模块的下载链接和信息查询！

点击下面的帮助按钮来查看使用方法。
加入 [我的频道](https://t.me/lsposed_Modules_Updates_Tracker) 获取关于 lsposed 模块的所有更新和公告！
"""
unsub_msg = """
<b>成功退订了</b> <code>{}</code> <b>的更新！</b>
"""
not_sub_msg = """
<b>你好像没有订阅</b> <code>{}</code> <b>的更新！</b>
"""
sub_msg = """
<b>成功订阅了</b> <code>{}</code> <b>的更新！</b>
"""
already_sub_msg = """
<b>已经订阅过</b> <code>{}</code> <b>的更新！</b>
"""
not_found_msg = """
<b>没有找到名为</b> <code>{}</code> <b>的模块！</b>
"""


def gen_help_button() -> InlineKeyboardMarkup:
    data_ = [[InlineKeyboardButton("📢 官方频道", url="https://t.me/lsposed_Modules_Updates_Tracker"),
              InlineKeyboardButton("💬 官方群组", url="https://t.me/Invite_Challenge_Bot?start=1"), ],
             [InlineKeyboardButton("❓ 阅读帮助", callback_data="help")],
             ]
    return InlineKeyboardMarkup(data_)


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["start"]))
async def start_command(client: Client, message: Message):
    """
        回应消息
    """
    if len(message.command) == 1:
        await message.reply(des.format(message.from_user.mention(),
                                       me.name,
                                       f"https://t.me/{me.username}"),
                            reply_markup=gen_help_button(),
                            quote=True, )
    else:
        data = message.command[1].replace("_", ".")
        if data.startswith("un-"):
            # 退订
            name = data[3:]
            data = from_name_to_module(name)
            if data:
                success = remove_from_subs(message.from_user.id, data)
                if success:
                    await message.reply(unsub_msg.format(data.name), quote=True)
                else:
                    await message.reply(not_sub_msg.format(data.name), quote=True)
            else:
                await message.reply(not_found_msg.format(name), quote=True)
        else:
            # 订阅
            name = data
            data = from_name_to_module(data)
            if data:
                success = add_to_subs(message.from_user.id, data)
                if success:
                    await message.reply(sub_msg.format(data.name), quote=True)
                else:
                    await message.reply(already_sub_msg.format(data.name), quote=True)
            else:
                await message.reply(not_found_msg.format(name), quote=True)
