from pyrogram import Client, filters
from pyrogram.types import Message

from defs.source import from_keyword_to_module
from defs.subs import gen_subs_msg, gen_back_button, add_to_subs, remove_from_subs
from plugins.start import sub_msg, not_found_msg, already_sub_msg, unsub_msg, not_sub_msg

sub_help_msg = """
👩🏻‍💼 » /subscribe <code>包名|关键词|作用域|开源地址|模块地址</code> - 订阅更新
     <code>/subscribe nil.nadph.qnotified</code>
     <code>/subscribe QNotified</code>
     <code>/subscribe com.tencent.mobileqq</code>
     <code>/subscribe https://github.com/ferredoxin/QNotified</code>
     <code>/subscribe https://modules.lsposed.org/module/nil.nadph.qnotified</code>
"""
unsub_help_msg = """
👩🏻‍💼 » /unsubscribe <code>包名|关键词|作用域|开源地址|模块地址</code> - 取消订阅更新
     <code>/unsubscribe nil.nadph.qnotified</code>
     <code>/unsubscribe QNotified</code>
     <code>/unsubscribe com.tencent.mobileqq</code>
     <code>/unsubscribe https://github.com/ferredoxin/QNotified</code>
     <code>/unsubscribe https://modules.lsposed.org/module/nil.nadph.qnotified</code>
"""


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["subscription"]))
async def subscription_command(_: Client, message: Message):
    text = gen_subs_msg(message.from_user.id)
    await message.reply(text, reply_markup=gen_back_button(), quote=True, )


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["subscribe"]))
async def sub_command(_: Client, message: Message):
    if len(message.command) == 1:
        await message.reply(sub_help_msg, reply_markup=gen_back_button(), quote=True)
    else:
        data = " ".join(message.command[1:])
        module = from_keyword_to_module(data)
        if module:
            success = add_to_subs(message.from_user.id, module)
            if success:
                await message.reply(sub_msg.format(module.name), quote=True)
            else:
                await message.reply(already_sub_msg.format(module.name), quote=True)
        else:
            await message.reply(not_found_msg.format(data), quote=True)


@Client.on_message(filters.incoming & filters.private &
                   filters.command(["unsubscribe"]))
async def un_sub_command(_: Client, message: Message):
    if len(message.command) == 1:
        await message.reply(unsub_help_msg, reply_markup=gen_back_button(), quote=True)
    else:
        data = " ".join(message.command[1:])
        module = from_keyword_to_module(data)
        if module:
            success = remove_from_subs(message.from_user.id, module)
            if success:
                await message.reply(unsub_msg.format(module.name), quote=True)
            else:
                await message.reply(not_sub_msg.format(module.name), quote=True)
        else:
            await message.reply(not_found_msg.format(data), quote=True)
